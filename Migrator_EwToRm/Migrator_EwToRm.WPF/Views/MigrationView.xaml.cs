﻿using System.Windows;
using System.Windows.Controls;

namespace Migrator_EwToRm.WPF.Views
{
    /// <summary>
    /// Interaction logic for MigrationView.xaml
    /// </summary>
    public partial class MigrationView : UserControl
    {
        public MigrationView()
        {
            InitializeComponent();
        }

        private void ButtonStartProcess_Click(object sender, RoutedEventArgs e)
        {
            this.ButtonStartProcess.IsEnabled = false;
        }
    }
}
