﻿using Migrator_EwToRm.WPF.ViewModels;
using System.Windows;
using System.Windows.Controls;

namespace Migrator_EwToRm.WPF.Views
{
    /// <summary>
    /// Interaction logic for ConnectionView.xaml
    /// </summary>
    public partial class ConnectionView : UserControl
    {
        public ConnectionView()
        {
            InitializeComponent();
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (this.DataContext != null)
            { 
                ((ConnectionViewModel)this.DataContext).Password = ((PasswordBox)sender).SecurePassword; 
            }
        }
    }
}
