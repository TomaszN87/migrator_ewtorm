﻿using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using Migrator_EwToRm.Core.Model;
using System;
using System.IO;

namespace Migrator_EwToRm.WPF.Tools
{
    internal static class PdfCreator
    {
        public static void MakeTableReport(string filePatch, ResultQuery reportTable)
        {
            using var pdfDocument = new PdfDocument(
                new PdfWriter(
                    new FileStream(filePatch, FileMode.Create, FileAccess.Write)));
            using var document = new Document(pdfDocument);

            var Head = "Raport Migracji bazy danych.";
            document.Add(new Paragraph(Head));
            var Subhead = $"Data: {DateTime.Now:dd-MM-yyyy HH:mm}";
            document.Add(new Paragraph(Subhead));

            if (reportTable.IsError)
            {
                var ErrorLinehead = $"Migracja nieudana: {reportTable.ErrorMessage}";
                document.Add(new Paragraph(ErrorLinehead));
            }
            else
            {
                var table = new Table(reportTable.Columns!.Count, true);

                foreach (var colName in reportTable.Columns)
                    table.AddCell(new Paragraph(colName));

                foreach (var row in reportTable.Rows!)
                    foreach (var cell in row)
                        table.AddCell(new Paragraph(cell));

                document.Add(table);

                document.Close();
                pdfDocument.Close();
            }
        }
    }
}
