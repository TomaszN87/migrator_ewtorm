﻿using Microsoft.Extensions.DependencyInjection;
using Migrator_EwToRm.Core.Services;
using Migrator_EwToRm.Infrastructure.Services;
using System;
using System.Windows;

namespace Migrator_EwToRm.WPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            Services = ConfigureServices();

            this.InitializeComponent();
        }

        public new static App Current => (App)Application.Current;

        public IServiceProvider Services { get; }

        private static IServiceProvider ConfigureServices()
        {
            var services = new ServiceCollection();

            services.AddSingleton<IDatabaseService, MSSQLDatabaseService>();

            return services.BuildServiceProvider();
        }
    }
}
