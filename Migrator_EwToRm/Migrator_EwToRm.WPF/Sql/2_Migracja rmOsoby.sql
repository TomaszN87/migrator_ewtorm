﻿--Tworzenie nowej tabeli [rm_osoba]
SELECT 
	IDENTITY (int, 1, 1) AS [IDosoba],
	CAST(pb_ewid2_osoba.[pesel] as nvarchar(11)) AS [pesel],
	CAST(pb_ewid2_rm_srp_xml.[data_akt] as datetime2(3)) AS [data_aktual],
	CAST(pb_ewid2_osoba.[do_dzr] as datetime2(3)) AS [data_zalrek],
	pb_ewid2_rm_srp_xml.[id] AS [IDsrp],
	CAST(pb_ewid2_osoba.[do_owop_kt] as nvarchar(7)) AS [owop_kt],
	CAST(pb_ewid2_osoba.[do_owop_ro] as nvarchar(100)) AS [owop_ro],
	CAST(pb_ewid2_osoba.[do_owop_kk] as nvarchar(4)) AS [owop_kk],
	CAST(pb_ewid2_osoba.[do_owop_koo] as nvarchar(112)) AS [owop_koo],
	CAST(pb_ewid2_osoba.[do_owop_koo_k] as nvarchar(3)) AS [owop_koo_k],
	CAST(NULL as nvarchar(3)) AS [owop_nu],
	pb_ewid2_osoba.[do_wr] AS [wersja_rek],
	--Jak sparsować wartość [do_pda] =  rrrr-01-01 00:00:00
	CAST(REPLACE([do_pda], 'rrrr', '0001') as datetime2(3)) AS [pesel_data_aktual],
	pb_ewid2_osoba.kodpob AS [kodpob],
	pb_ewid2_osoba.[status_zgodn] AS [status_zgodn],
	pb_ewid2_rm_srp_xml.[sz_dps] AS [sz_dps],
	pb_ewid2_rm_srp_xml.[sz_dpc] AS [sz_dpc],
	pb_ewid2_rm_srp_xml.[sz_di] AS [sz_di],
	pb_ewid2_rm_srp_xml.[sz_dn] AS [sz_dn],
	CAST(NULL as int) AS [rej_wyb],
	CAST(
		 CASE
			WHEN pb_ewid2_ew_rej_skres.[kod_dec] = '1' THEN 'R41'
			WHEN pb_ewid2_ew_rej_skres.[kod_dec] = '2' THEN 'R41b'
			WHEN pb_ewid2_ew_rej_skres.[kod_dec] = '3' THEN 'R42'
			WHEN pb_ewid2_ew_rej_skres.[kod_dec] = '4' THEN 'R43'
			ELSE NULL
		END as nvarchar(4)) AS [rej_skr],
	CAST(
		 CASE
			WHEN pb_ewid2_ew_rej_dod.[uwagi] = '1' THEN 'Z2A'
			WHEN pb_ewid2_ew_rej_dod.[uwagi] = '2' THEN 'Z2C'
			WHEN pb_ewid2_ew_rej_dod.[uwagi] = '3' THEN 'Z2B'
			ELSE NULL
		END as nvarchar(3)) AS [rej_dop],
	pb_ewid2_rm_srp_xml.[xml] AS [xml],
	CAST(pb_ewid2_rm_srp_xml.[data_pob] as datetime2(3)) AS [data_pob],
	CAST(pb_ewid2_rm_srp_xml.[blok_data] as datetime2(3)) AS [data_blok],
	CAST(pb_ewid2_rm_srp_xml.[blok_uzyt] as nvarchar(200)) AS [uzyt_blok],
	CAST((pb_ewid2_osoba.rr_wpr+'-'+pb_ewid2_osoba.mm_wpr+'-'+pb_ewid2_osoba.dd_wpr) as datetime2(3)) AS [data_kre],--Nie może być NOT NULL, ponieważ są rekord z NULL w bazie żródłowej
	CAST((pb_ewid2_osoba.rr_mod+'-'+pb_ewid2_osoba.mm_mod+'-'+pb_ewid2_osoba.dd_mod) as datetime2(3)) AS [data_mod],
	CAST(pb_ewid2_osoba.[id_kto] as nvarchar(200)) AS [uzyt_kre],
	CAST(pb_ewid2_osoba.[id_kto] as nvarchar(200)) AS [uzyt_mod]
INTO 
	[PB_EWID3].[dbo].[rm_osoba]
FROM
	[dbo].[ew_osoba] pb_ewid2_osoba
LEFT JOIN
	[dbo].[rm_srp_xml] pb_ewid2_rm_srp_xml ON pb_ewid2_osoba.pesel = pb_ewid2_rm_srp_xml.pesel
LEFT JOIN
	[dbo].[ew_rej_skres] pb_ewid2_ew_rej_skres ON pb_ewid2_osoba.pesel = pb_ewid2_ew_rej_skres.pesel
LEFT JOIN
	[dbo].[ew_rej_dod] pb_ewid2_ew_rej_dod ON pb_ewid2_osoba.pesel = pb_ewid2_ew_rej_dod.pesel

-- Zmiana pól na NOT NULL
ALTER TABLE
	[PB_EWID3].[dbo].[rm_osoba]
ALTER COLUMN
	[uzyt_kre] nvarchar(200) NOT NULL;

-- Dodanie Opisów
USE [PB_EWID3];

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'Numer Pesel',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'pesel'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'Data aktualizacji w srp',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'data_aktual'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'Data założenia rekordu w srp',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'data_zalrek'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'Id osoby w srp',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'IDsrp'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'Organ wnioskujacy o Pesel - kod terytorialny',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'owop_kt'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'Organ wnioskujacy o Pesel - rodzaj organu',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'owop_ro'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'Organ wnioskujacy o Pesel - kod konsula',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'owop_kk'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'Prgan wnioskujacy o Pesel - kraj Organu Obcego',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'owop_koo'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'Organ wnioskujacy o Pesel - kraj organu obcego - kod',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'owop_koo_k'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'Organ wnioskujacy o Pesel - numer USC',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'owop_nu'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'wersja Rekordu',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'wersja_rek'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'Numer Pesel Data Aktualizacji',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'pesel_data_aktual'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'Kod pobytu w gminie',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'kodpob'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'status zgodności danych SRP vs. RM',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'status_zgodn'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'status zgodności danych pobytu stałego SRP vs. RM',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'sz_dps'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'status zgodności danych pobytu czasowego SRP vs. RM',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'sz_dpc'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'status zgodności danych imion SRP vs. RM',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'sz_di'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'status zgodności danych nazwiska SRP vs. RM',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'sz_dn'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'status w rejestrze wyborców',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'rej_wyb'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'status w rejestrze skreśleń',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'rej_skr'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'status w rejestrze dopisanych',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'rej_dop'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'ostatni pobrany xml',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'xml'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'data pobrania xml-a',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'data_pob'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'data zablokowania rekordu',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'data_blok'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'użytkownik blokujący rekord',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'uzyt_blok'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'data utworzenia rekordu w pb_ewid',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'data_kre'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'data modyfikacji rekordu w pb_ewid',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'data_mod'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'użytkownik tworzący rekord w pb_ewid',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'uzyt_kre'

EXEC sp_addextendedproperty 
    @name = N'Opis', @value = 'użytkownik ostatnio modyfikujący w pb_ewid',
    @level0type = N'Schema', @level0name = 'dbo',
    @level1type = N'Table', @level1name = 'rm_osoba', 
    @level2type = N'Column', @level2name = 'uzyt_mod'