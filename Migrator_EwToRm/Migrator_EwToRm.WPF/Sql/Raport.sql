﻿--Raport
SELECT
	'ew_osoba' AS 'TABELA ZRODLOWA',
	(SELECT COUNT(*) FROM [dbo].[ew_osoba]) AS 'POZYCJE',
	'rm_osoba' AS 'TABELA DOCELOWA',
	(SELECT COUNT(*) FROM [PB_EWID3].[dbo].[rm_osoba]) AS 'PO MIGTACJI'
UNION ALL
SELECT
	'ew_innaTabSrc' AS 'TABELA ZRODLOWA',
	(SELECT COUNT(*) FROM [dbo].[ew_osoba]) AS 'POZYCJE',
	'rm_innaTabDsc' AS 'TABELA DOCELOWA',
	(SELECT COUNT(*) FROM [PB_EWID3].[dbo].[rm_osoba]) AS 'PO MIGTACJI';