﻿UPDATE ew_dowody 
	SET pesel = b.pesel 
	FROM [dbo].[ew_dowody] a 
	LEFT JOIN [dbo].[ew_osoba] b ON a.idosoba = b.idosoba 
	WHERE a.pesel <> b.pesel;

UPDATE [dbo].[ew_impop] 
	SET pesel = b.pesel 
	FROM [dbo].[ew_impop] a 
	LEFT JOIN ew_osoba b ON a.idosoba = b.idosoba 
	WHERE a.pesel <> b.pesel;

