﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using Microsoft.Toolkit.Mvvm.Messaging;
using Microsoft.Win32;
using Migrator_EwToRm.Core.Model;
using Migrator_EwToRm.Core.Services;
using Migrator_EwToRm.Infrastructure.Services;
using Migrator_EwToRm.WPF.Tools;
using Migrator_EwToRm.WPF.ViewModels.Messages;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Migrator_EwToRm.WPF.ViewModels
{
    public class MigrationViewModel : ObservableRecipient
    {
        private readonly IScriptsService _scriptsService;
        private readonly IDatabaseService _databaseService;
        private ResultQuery? _resultQuery;

        public MigrationViewModel()
        {
            _databaseService = App.Current.Services.GetService<IDatabaseService>()!;
            _scriptsService = new FileScriptsService();
            _resultQuery = null;

            Messenger.Register<ChangedLoginDataMessage>(
                this,
                async (r, msg) =>
                {
                    await _scriptsService.LoadScriptsFromFolder(@"Sql");
                    Scripts = new ObservableCollection<ScriptsViewModel>(
                            _scriptsService.Scripts
                                .Select(s => new ScriptsViewModel(s, _databaseService))
                        );
                });

            StartProcessCommand = new AsyncRelayCommand(StartProcess);
            MakePdfReportCommand = new RelayCommand(MakePdfReport);
        }

        private ObservableCollection<ScriptsViewModel> _scripts = new();
        public ObservableCollection<ScriptsViewModel> Scripts
        {
            get => _scripts;
            set => SetProperty(ref _scripts, value);
        }

        private bool _processDone;
        public bool ProcessDone
        {
            get => _processDone;
            set => SetProperty(ref _processDone, value);
        }

        public IAsyncRelayCommand StartProcessCommand { get; }
        private async Task StartProcess()
        {
            try
            {
                foreach (var scriptVM in Scripts)
                {
                    _resultQuery = await scriptVM.RunScript();
                    if (_resultQuery.IsError)
                        break;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                ProcessDone = true;
            }
        }

        public IRelayCommand MakePdfReportCommand { get; }
        private void MakePdfReport()
        {
            if (_resultQuery == null)
                throw new NullReferenceException(nameof(_resultQuery));

            if (_resultQuery.IsError == false && _resultQuery.HasTable == false)
            {
                MessageBox.Show(
                    "Brak pliku sql generującego raport z migracji.",
                    "Informacja",
                    MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            var saveFileDialog = new SaveFileDialog()
            {
                Filter = "Pdf file (*.pdf)|*.pdf"
            };

            if (saveFileDialog.ShowDialog() == true)
                PdfCreator.MakeTableReport(saveFileDialog.FileName, _resultQuery);
        }
    }
}
