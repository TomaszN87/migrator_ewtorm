﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using Microsoft.Toolkit.Mvvm.Messaging;
using Migrator_EwToRm.Core.Services;
using Migrator_EwToRm.Infrastructure.Exceptions;
using Migrator_EwToRm.WPF.ViewModels.Messages;
using System.Collections.ObjectModel;
using System.Security;
using System.Threading.Tasks;
using System.Windows;

namespace Migrator_EwToRm.WPF.ViewModels
{
    internal class ConnectionViewModel : ObservableRecipient
    {
        private readonly IDatabaseService _databaseService;

        public ConnectionViewModel()
        {
            _databaseService = App.Current.Services.GetService<IDatabaseService>()!;
            _serverNames = new ObservableCollection<string>(
                _databaseService.GetMsSqlServers());

            ConnectToDbCommand = new AsyncRelayCommand(ConnectToDbAsync);
        }

        private ObservableCollection<string> _serverNames;
        public ObservableCollection<string> ServerNames
        {
            get => _serverNames;
            set => SetProperty(ref _serverNames, value);
        }

        private string _server = string.Empty;
        public string Server
        {
            get => _server;
            set => SetProperty(ref _server, value);
        }

        private string _user = string.Empty;
        public string User
        {
            get => _user;
            set => SetProperty(ref _user, value);
        }

        private string _scrDatabase = string.Empty;
        public string ScrDatabase
        {
            get => _scrDatabase;
            set => SetProperty(ref _scrDatabase, value);
        }

        private string _dscDatabase = string.Empty;
        public string DscDatabase
        {
            get => _dscDatabase;
            set => SetProperty(ref _dscDatabase, value);
        }

        private SecureString _password = new();
        public SecureString Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        public IAsyncRelayCommand ConnectToDbCommand { get; }
        private async Task ConnectToDbAsync()
        {
            try
            {
                await _databaseService.ConnectToDb(
                    new Core.Model.DataBaseInfo(
                        Server,
                        User,
                        Password,
                        ScrDatabase,
                        DscDatabase));

                Messenger.Send(new ChangedLoginDataMessage(User));
            }
            catch(LoginException loginEx)
            {
                MessageBox.Show(
                    loginEx.Message, 
                    "Błąd logowania", 
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
