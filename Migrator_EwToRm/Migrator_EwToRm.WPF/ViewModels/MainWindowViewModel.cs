﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using Microsoft.Toolkit.Mvvm.Messaging;
using Migrator_EwToRm.WPF.ViewModels.Messages;
using System.Windows.Input;

namespace Migrator_EwToRm.WPF.ViewModels
{
    public class MainWindowsViewModel : ObservableRecipient
    {
        public MainWindowsViewModel()
        {
            ChangeViewCommand = new RelayCommand(ChangeView);
            Messenger.Register<ChangedLoginDataMessage>(
                this, 
                (r, msg) =>
                {
                    ChangeView();
                });
        }

        private int _selectedViewIndex;
        public int SelectedViewIndex
        {
            get => _selectedViewIndex;
            set => SetProperty(ref _selectedViewIndex, value);
        }

        public ICommand ChangeViewCommand { get; }
        private void ChangeView()
        {
            SelectedViewIndex++;
        }
    }
}
