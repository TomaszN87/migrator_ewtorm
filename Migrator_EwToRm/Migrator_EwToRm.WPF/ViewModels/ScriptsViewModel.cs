﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using Migrator_EwToRm.Core.Model;
using Migrator_EwToRm.Core.Services;
using System;
using System.Threading.Tasks;

namespace Migrator_EwToRm.WPF.ViewModels
{
    public class ScriptsViewModel : ObservableObject
    {
        private readonly Script _script;
        private readonly IDatabaseService _databaseService;

        public ScriptsViewModel(Script script, IDatabaseService databaseService)
        {
            _script = script;
            _databaseService = databaseService;

            Index = _script.Order;
            _name = _script.Name;
            _status = ScriptState.Pending;
            _resultMessage = string.Empty;
            Progress = 0;
        }

        private int _index;
        public int Index
        {
            get => _index;
            set => SetProperty(ref _index, value);
        }

        private string _name;
        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        private ScriptState _status;
        public ScriptState Status
        {
            get => _status;
            set => SetProperty(ref _status, value);
        }

        private int _progress;
        public int Progress
        {
            get => _progress;
            set => SetProperty(ref _progress, value);
        }

        private string _resultMessage;
        public string ResultMessage
        {
            get => _resultMessage;
            set => SetProperty(ref _resultMessage, value);
        }

        private string _time = string.Empty;
        public string Time
        {
            get => _time;
            set => SetProperty(ref _time, value);
        }

        public async Task<ResultQuery> RunScript()
        {
            ResultQuery resultReport;

            try
            {
                Status = ScriptState.InProgress;
                if(Index == 0)
                {
                    resultReport = await _databaseService.RunScriptWithResult(_script);
                    ResultMessage = $"OK, Kolumny: {resultReport.Columns!.Count}, Wiersze: {resultReport.Rows!.Count}";
                }
                else
                {
                    resultReport = await _databaseService.RunScriptNonQuery(_script);
                    ResultMessage = "OK";
                }

                Status = ScriptState.Completed;
            }
            catch (Exception ex)
            {
                resultReport = new ResultQuery($"{_script.Name}: {ex.Message}");
                ResultMessage = $"Błąd: {ex.Message}";
                Status = ScriptState.Error;
            }

            return resultReport;
        }
    }

    public enum ScriptState
    {
        Pending,
        InProgress,
        Completed,
        Error,
    }
}
