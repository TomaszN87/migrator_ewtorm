﻿using Microsoft.Toolkit.Mvvm.Messaging.Messages;

namespace Migrator_EwToRm.WPF.ViewModels.Messages
{
    public sealed class ChangedLoginDataMessage : ValueChangedMessage<string>
    {
        public ChangedLoginDataMessage(string user) : base(user)
        {
        }
    }
}
