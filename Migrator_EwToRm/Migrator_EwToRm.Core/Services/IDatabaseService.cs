﻿using Migrator_EwToRm.Core.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Migrator_EwToRm.Core.Services
{
    public interface IDatabaseService
    {
        Task ConnectToDb(DataBaseInfo dataBaseInfo);
        IEnumerable<string> GetMsSqlServers();
        Task<ResultQuery> RunScriptNonQuery(Script script);
        Task<ResultQuery> RunScriptWithResult(Script script);
    }
}
