﻿using Migrator_EwToRm.Core.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Migrator_EwToRm.Core.Services
{
    public interface IScriptsService
    {
        IEnumerable<Script> Scripts { get; }
        Task LoadScriptsFromFolder(string patch);
    }
}
