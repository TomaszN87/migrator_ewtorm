﻿using System.Security;

namespace Migrator_EwToRm.Core.Model
{
    public struct DataBaseInfo
    {
        public DataBaseInfo(
            string server, 
            string user, 
            SecureString password, 
            string sourceDatabase, 
            string destinationDatabase)
        {
            ServerName = server;
            User = user;
            Password = password;
            InitialCatalog = sourceDatabase;
            DestinationDatabase = destinationDatabase;
        }

        public string ServerName { get; }
        public string User { get; set; }
        public SecureString Password { get; } 
        public string InitialCatalog { get; }
        public string DestinationDatabase { get; }
    }
}
