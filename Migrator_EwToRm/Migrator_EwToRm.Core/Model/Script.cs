﻿namespace Migrator_EwToRm.Core.Model
{
    public class Script
    {
        private readonly string _scriptTxt;

        public Script(string name, int order, string contentTxt)
        {
            _scriptTxt = contentTxt;
            Name = name;
            Order = order;
        }

        public string Name { get; private set; }
        public int Order { get; private set; }

        public string Compile(string destinationBase)
        {
            return _scriptTxt
                .Replace("[PB_EWID3]", $"[{destinationBase}]");
        }
    }
}
