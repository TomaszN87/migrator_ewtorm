﻿using System;
using System.Collections.Generic;

namespace Migrator_EwToRm.Core.Model
{
    public class ResultQuery
    {
        private readonly List<string[]>? _rows;

        public ResultQuery()
        {
            HasTable = false;
        }

        public ResultQuery(string[] columns)
        {
            HasTable = true;
            Columns = new List<string>(columns);
            _rows = new List<string[]>();
        }

        public ResultQuery(string errorMessage)
        {
            HasTable = false;
            ErrorMessage = errorMessage;
        }

        public ICollection<string>? Columns { get; private set; }
        public ICollection<string[]>? Rows => _rows;
        public string? ErrorMessage { get; }
        public bool HasTable { get; }

        public bool IsError => string.IsNullOrWhiteSpace(ErrorMessage) == false;

        public void AddRow(string[] values)
        {
            if(Rows == null || Columns == null)
                throw new ArgumentNullException(
                    $"{nameof(Rows)}, {nameof(Columns)}",
                    "Brak tabeli.");

            if (values.Length != Columns.Count)
                throw new ArgumentException("Nieprawidłowa liczba argumentów");
            Rows.Add(values);
        }
    }
}
