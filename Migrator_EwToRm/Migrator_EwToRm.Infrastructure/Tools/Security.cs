﻿using System;
using System.Net;
using System.Runtime.InteropServices;
using System.Security;

namespace Migrator_EwToRm.Infrastructure.Tools
{
    internal static class Security
    {
        internal static string SecureStringToString(SecureString value)
        {
            IntPtr valuePtr = IntPtr.Zero;
            try
            {
                valuePtr = Marshal.SecureStringToGlobalAllocUnicode(value);
                return Marshal.PtrToStringUni(valuePtr);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(valuePtr);
            }
        }

        internal static SecureString StringToSecureString(string password)
        {
            if (password == null)
                throw new ArgumentNullException(password);

            var securePassword = new SecureString();

            foreach (char c in password)
                securePassword.AppendChar(c);

            securePassword.MakeReadOnly();
            return securePassword;
        }
    }
}
