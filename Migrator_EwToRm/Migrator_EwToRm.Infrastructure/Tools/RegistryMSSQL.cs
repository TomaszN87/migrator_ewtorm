﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Migrator_EwToRm.Infrastructure.Tools
{
    internal static class RegistryMSSQL
    {
        public static IEnumerable<string> GetServers()
        {
            RegistryView registryView =
                Environment.Is64BitOperatingSystem ?
                    RegistryView.Registry64 :
                    RegistryView.Registry32;

            using RegistryKey hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, registryView);
            RegistryKey? instanceKey =
                hklm.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server\Instance Names\SQL", false);

            return
                instanceKey != null ?
                    instanceKey.GetValueNames()
                    .Select(name => $"{Environment.MachineName}\\{name}") :
                     Enumerable.Empty<string>();
        }
    }
}
