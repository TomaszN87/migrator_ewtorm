﻿using System;

namespace Migrator_EwToRm.Infrastructure.Exceptions
{
    public class FileNameFormatException : Exception
    {
        public FileNameFormatException()
        {
        }

        public FileNameFormatException(string message)
            : base(message)
        {
        }

        public FileNameFormatException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }

}
