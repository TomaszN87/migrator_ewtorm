﻿using Migrator_EwToRm.Core.Model;
using Migrator_EwToRm.Core.Services;
using Migrator_EwToRm.Infrastructure.Tools;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Migrator_EwToRm.Infrastructure.Services
{
    public class MSSQLPreLoginDatabaseService : IDatabaseService
    {
        private readonly MSSQLDatabaseService _baseDatabaseService;

        public MSSQLPreLoginDatabaseService()
        {
            _baseDatabaseService = new MSSQLDatabaseService();
        }

        public async Task ConnectToDb(DataBaseInfo dataBaseInfo)
        {
            var preLoginDataBaseInfo = new DataBaseInfo(
                dataBaseInfo.ServerName,
                "sa",
                Security.StringToSecureString("testbazy2022"),
                "PB_EWID2",
                "PB_EWID3");

            await _baseDatabaseService.ConnectToDb(preLoginDataBaseInfo);
        }

        public IEnumerable<string> GetMsSqlServers()
        {
            return _baseDatabaseService.GetMsSqlServers();
        }

        public async Task<ResultQuery> RunScriptNonQuery(Script script)
        {
            return await _baseDatabaseService.RunScriptNonQuery(script);
        }
        
        public async Task<ResultQuery> RunScriptWithResult(Script script)
        {
            return await _baseDatabaseService.RunScriptWithResult(script);
        }
    }
}
