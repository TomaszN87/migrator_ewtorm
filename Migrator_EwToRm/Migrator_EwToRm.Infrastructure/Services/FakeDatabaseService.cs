﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Migrator_EwToRm.Core.Services;
using Migrator_EwToRm.Infrastructure.Exceptions;
using Migrator_EwToRm.Infrastructure.Tools;
using Migrator_EwToRm.Core.Model;

namespace Migrator_EwToRm.Infrastructure.Services
{
    public class FakeDatabaseService : IDatabaseService
    {
        public IEnumerable<string> GetMsSqlServers()
        {
            return RegistryMSSQL.GetServers();
        }

        public static Task CheckConnectToDbAsync(DataBaseInfo dataBaseInfo)
        {
            if (dataBaseInfo.User == "su")
                return Task.CompletedTask;
            throw new LoginException("Nieprawidłowa nazwa użytkownika.");
        }

        public Task ConnectToDb(DataBaseInfo dataBaseInfo)
        {
            throw new NotImplementedException();
        }

        public Task<ResultQuery> RunScriptNonQuery(Script script)
        {
            throw new NotImplementedException();
        }

        public Task<ResultQuery> RunScriptWithResult(Script script)
        {
            throw new NotImplementedException();
        }

        public Task Roolback()
        {
            throw new NotImplementedException();
        }
    }
}
