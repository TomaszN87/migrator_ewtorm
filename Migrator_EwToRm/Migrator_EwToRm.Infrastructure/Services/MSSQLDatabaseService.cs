﻿using Migrator_EwToRm.Core.Model;
using Migrator_EwToRm.Core.Services;
using Migrator_EwToRm.Infrastructure.Exceptions;
using Migrator_EwToRm.Infrastructure.Tools;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Migrator_EwToRm.Infrastructure.Services
{
    public class MSSQLDatabaseService : IDatabaseService
    {
        private DataBaseInfo _dataBaseInfo;
        private SqlConnection? _connection;

        public IEnumerable<string> GetMsSqlServers()
        {
            return RegistryMSSQL.GetServers();
        }

        public async Task<ResultQuery> RunScriptNonQuery(Script script)
        {
            if (_connection == null)
                throw new System.NullReferenceException("Nie połączono z bazą danych. SqlConnection is NULL.");

            await _connection.OpenAsync();

            using var command = _connection.CreateCommand();
            command.CommandText = script.Compile(_dataBaseInfo.DestinationDatabase);

            await command.ExecuteNonQueryAsync();

            await _connection.CloseAsync();

            return new ResultQuery();
        }

        public async Task<ResultQuery> RunScriptWithResult(Script script)
        {
            if (_connection == null)
                throw new System.NullReferenceException("Nie połączono z bazą danych. SqlConnection is NULL.");

            await _connection.OpenAsync();

            using var command = _connection.CreateCommand();
            command.CommandText = script.Compile(_dataBaseInfo.DestinationDatabase);

            using SqlDataReader reader = await command.ExecuteReaderAsync();

            var columntNames = new List<string>();
            for (int i = 0; i < reader.FieldCount; i++)
                columntNames.Add(reader.GetName(i));

            var queryResult = new ResultQuery(columntNames.ToArray());
            while (reader.Read())
            {
                var values = new object[columntNames.Count];
                reader.GetValues(values);
                queryResult.AddRow(
                    values.Select(x => x.ToString()!).ToArray());
            }

            await _connection.CloseAsync();

            return queryResult;
        }

        public Task ConnectToDb(DataBaseInfo dataBaseInfo)
        {
            _dataBaseInfo = dataBaseInfo;

            var builder = new SqlConnectionStringBuilder
            {
                DataSource = _dataBaseInfo.ServerName,
                UserID = _dataBaseInfo.User,
                Password = Security.SecureStringToString(_dataBaseInfo.Password),
                InitialCatalog = _dataBaseInfo.InitialCatalog,
            };

            try
            {
                _connection = new SqlConnection(builder.ConnectionString);
            }
            catch (SqlException ex)
            {
                throw new LoginException(ex.Message, ex);
            }

            return Task.CompletedTask;
        }
    }
}
