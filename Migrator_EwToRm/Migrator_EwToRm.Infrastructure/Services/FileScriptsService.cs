﻿using Migrator_EwToRm.Core.Model;
using Migrator_EwToRm.Core.Services;
using Migrator_EwToRm.Infrastructure.Exceptions;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Migrator_EwToRm.Infrastructure.Services
{
    public class FileScriptsService : IScriptsService
    {
        public IEnumerable<Script> Scripts { get; private set;} = Enumerable.Empty<Script>();

        public async Task LoadScriptsFromFolder(string patch)
        {
            var fileNames = Directory.GetFiles(patch, "*.sql");
            var tasks = fileNames
                .Select(async patch =>
                {
                    var scriptTxt = await File.ReadAllTextAsync(patch);
                    var fileName = patch
                        .Split("\\").Last()
                        .Replace(".sql", string.Empty);

                    if(fileName == "Raport")
                        return new Script(fileName, 0, scriptTxt);

                    var sectionFileName = fileName.Split('_');
                    if(sectionFileName.Length < 2)
                        throw new FileNameFormatException("Brak sekcji w nazwie pliku.");
                    var isInt = int.TryParse(sectionFileName.First(), out var order);
                    if(isInt == false || order == 0)
                        throw new FileNameFormatException("Nieprawdłowy format pierwszej sekcji.");
                    var scriptName = sectionFileName[1];

                    return new Script(scriptName, order, scriptTxt);
                }).ToArray();

            var scripts = await Task.WhenAll(tasks);
            Scripts = new List<Script>(
                scripts.OrderBy(x => x.Order > 0 ? x.Order : int.MaxValue));
        }
    }
}
